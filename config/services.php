<?php

return array(
    'mandrill' => [
        'secret' => env('MANDRILL_API_KEY'),
    ],
);
