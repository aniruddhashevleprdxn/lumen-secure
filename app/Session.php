<?php

namespace App;

use Illuminate\Support\Facades\Hash;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Session extends Model
{

		protected $primaryKey = 'session_id';

		public function setSession($data) {

				$token = md5(uniqid(rand(), TRUE));

				$this->user_id = $data;
				$this->session_token = $token;
				$this->save();

		}

		public function logout($token) {

				if ($this->where('session_token', $token)->delete($token)) {
						return true;
				}
				else { return false; }

		}

}
