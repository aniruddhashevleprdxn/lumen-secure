<?php

namespace App;

use Illuminate\Support\Facades\Hash;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Role extends Model
{

	protected $primaryKey = 'role_id';

	public function insert($id) {

			$this->user_id = $id;
			$this->role = "user";

			$this->save();

	}

}
