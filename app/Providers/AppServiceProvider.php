<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    		$this->singleton('mailer', function () {
    		            $this->configure('services');
    		            return $this->loadComponent('mail', 'Illuminate\Mail\MailServiceProvider', 'mailer');
    		        });
    }
}
