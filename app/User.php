<?php

namespace App;

use Illuminate\Support\Facades\Hash;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model
{

	protected $primaryKey = 'id';

	public function insert($name, $password, $email) {

			$password = Hash::make($password);

			$this->name = $name;
			$this->email = $email;
			$this->password = $password;

			$this->save();

			return $this->id;

	}

	public function check($name, $password) {

			$actual_password = User::where('name',$name)->get()->first()['password'];
			if(Hash::check($password, $actual_password)) {
				return true;
			}
			else { return false; }

	}

	public function getData($name) {

			$user_id = User::select('id')->where('name',$name)->get()->first();
			return $user_id;

	}

	public function displayUser($token) {

			$user_info = User::select('id', 'name')->where('remember_token',$token)->get()->first();
			return $user_info;

	}


}
