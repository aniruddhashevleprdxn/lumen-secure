<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Session;
use App\Http\Controllers\Format;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function register(Request $req) {

        // Validation
        $this->validate($req, [
                'name' => 'required|min:4|max:120',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:4|confirmed',
                'password_confirmation' => 'required|min:4',
            ]);

        $name = $req['name'];
        $password = $req['password'];
        $email = $req['email'];

        $user = new User();
        $role = new Role();
        $session = new Session();

        // Add a User
        $user_id = $user->insert($name, $password, $email);
        // Add a resp Role
        $role->insert($user_id);
        // Add user in session as a loged in user
        $session->setSession($user_id);

        // Send a mail to User
        Mail::send('thankyou', ['name' => $name], function($message) use ($name, $email) {
                    $message->from('aniruddha.shevle.prdxn@gmail.com', 'Aniruddha Shevle')
                            ->subject('Thank you for Registration')
                            ->to($email, $name);
                });


        // Send a mail to Admin
        Mail::send('newUser', ['name' => $name, 'email' => $email ], function($message) use ($name) {
                    $message->from('aniruddha.shevle.prdxn@gmail.com', 'Client Name')
                            ->subject( "A New Contact Request from  ".$name)
                            ->to('aniruddha.shevle.prdxn@gmail.com', 'Client Name');
                });

        $format = new Format();

        return response()->json($format->formatJson("Mail sent", 200));

    }

    public function welcome() {
        $format = new Format();
        return response()->json($format->formatJson("Welcome to Home", 200));
    }

    public function login(Request $req) {

        $this->validate($req, [
                'name' => 'required|min:4|max:120',
                'password' => 'required|min:4'
            ]);

        $name = $req['name'];
        $password = $req['password'];

        $user = new User();
        $format = new Format();
        $session = new Session();

        if($user->check($name, $password)) {

            $user_id = $user->getData($name);

            $session->setSession($user_id);

            $data = "Welcome";
            return response()->json($format->formatJson($data, 200));
        }
        else {
            $data = "Please try again";
            return response()->json($format->formatJson($data, 400));
        }

    }

     public function displayUser(Request $req) {

        $token = $req->header('token');

        $user = new User();
        $format = new Format();

        $data = $user->displayUser($token);

        if( is_null($data) ) {
            return response()->json($format->formatJson($data, 400));
        }

        return response()->json($format->formatJson($data, 200));

     }

     public function settings(Request $req) {

        $token = $req->header('token');

        $user = new User();
        $format = new Format();

        $data = $user->displayUser($token);

        if( is_null($data) ) {
            return response()->json($format->formatJson($data, 400));
        }

        return response()->json($format->formatJson($data, 200));

     }

     public function logout(Request $req) {

        $token = $req->header('token');

        $session = new Session();
        $format = new Format();

        if ( $session->logout($token) ) {
            return response()->json($format->formatJson("Succsessfully loged out", 200));
        }

        return response()->json($format->formatJson("Something is wrong", 200));

     }


}
