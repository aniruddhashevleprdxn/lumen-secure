<?php

	namespace App\Http\Controllers;

class Format {

		public function formatJson($data, $code) {

				switch ($code) {

					case 200: $msg = "OK";
										$error = 0;
									  break;

					case 400: $msg = "Bad Request";
										$error = 1;
									  break;

					case 401: $msg = "Unauthorized";
										$error = 1;
									  break;

					case 403: $msg = "Forbidden";
										$error = 1;
									  break;

					case 404: $msg = "Not Found";
										$error = 1;
									  break;

					case 500: $msg = "Internal Server Error";
										$error = 1;
									  break;

				}

				$response = [
						'data' => $data,
						'meta' => [
								'code' => $code,
								'message' => $msg,
								'error' => $error,
								'type' => 'HTTP'
						]
				];

				return $response;

		}

}

