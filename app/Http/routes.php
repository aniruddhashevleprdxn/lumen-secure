<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Mail;

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->post('register', [
	'as' => 'register',
	'uses' => 'LoginController@register'
	]);

$app->post('login', 'LoginController@login');

$app->get('home', [
	'middleware' => 'user',
	'uses' => 'LoginController@displayUser'
	]);

$app->post('settings', [
	'middleware' => ['user','admin'],
	'uses' => 'LoginController@settings'
	]);

$app->get('welcome', [
	'as' => 'verified',
	'uses' => 'LoginController@welcome'
	]);


$app->post('logout', [
	'middleware' => 'user',
	'uses' => 'LoginController@logout'
	]);

$app->post('/email-test', function() use ($app) {

	Mail::send('thankyou',['name'=>'Aniruddha'], function($msg) {

			$msg->to('aniruddha.shevle.prdxn@gmail.com', 'Aniruddha')->subject('Welcome');

	});

	return response()->json("Mail Sent");

});
