<?php

namespace App\Http\Middleware;

use Closure;
use App\Session;
use App\Http\Controllers\Format;

class AuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = $request->header('token');

        $format = new Format();

        if ( is_null($token) ) {
            return response()->json($format->formatJson(null, 400));
        }

        $getToken = Session::where('session_token', $token)->get()->first();

        if(is_null($getToken)) {
            return response()->json($format->formatJson(null, 400));
        }

        return $next($request);

    }
}
