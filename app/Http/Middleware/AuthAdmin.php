<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use App\Http\Controllers\Format;
use App\Session;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('token');

        $sessionToken = Session::where("session_token",$token)->get()->first();

        $userId = Session::where("session_token",$token)->get()->first()['user_id'];

        $role = Role::where("user_id",$userId)->get()->first()['role'];

        if ( $role != "admin") {
            $format = new Format();
           return response()->json($format->formatJson(null, 401));
        }

        return $next($request);
    }
}
