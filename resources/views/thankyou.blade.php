<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
		<div>
				<p>
						Thanks for creating an account with the verification demo app.
		        Please follow the link below to verify your email address
						<a href={{ route('verified') }}>Welcome {{$name}}</a>
				</p>
		</div>
</body>
</html>
